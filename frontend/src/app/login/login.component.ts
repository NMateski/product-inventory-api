import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthorizationService } from '../shared/services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formGroup: FormGroup;

  constructor(private router: Router, private authorizationService: AuthorizationService) { 
    this.formGroup = new FormGroup({
      userName: new FormControl(null, [
        Validators.email,
        Validators.required,
        Validators.maxLength(255)
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.maxLength(40)
      ]),
    });
  }

  ngOnInit(): void {
  }

  public onSubmit(): void {
    if(this.formGroup.invalid) {
      console.log('Form is not valid!');
      console.log(this.formGroup.errors);
      return;
    }

    const userName = this.formGroup.value.userName;
    const password = this.formGroup.value.password;

    this.authorizationService.login(userName, password);

    // console.log(`Username: ${userName}, Password: ${password}`);

    // this.router.navigateByUrl('/inventories');
  }

}
