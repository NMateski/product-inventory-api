export class Page<T> {
    constructor(
        public content: T[],
        public totalNumberOfItems: number,
        public totalElements: number) {}
}