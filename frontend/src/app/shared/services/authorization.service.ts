import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

const API_AUTH = 'oauth/token';
const encodedKey = btoa('public-client:some-secret');
const headers = new HttpHeaders({
    'Content-type': 'application/x-www-form-urlencoded',
    'Authorization': `Basic ${encodedKey}`,
    'Accept': 'application/json'
});

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private httpClient: HttpClient, private router: Router) { }

  public login(username: string, password: string): void {
    const requestParams = new HttpParams()
      .append('username', username)
      .append('password', password)
      .append('grant_type', 'password');

    this.httpClient.post(`${environment.domainUrl}/${API_AUTH}`, requestParams ,{headers: headers}).subscribe({
      next: response => {
        localStorage.setItem('authorization', JSON.stringify(response));
        this.router.navigateByUrl('inventories');
      },
      error: error => console.error(error)
    });
  }

  public isLogedIn(): boolean {
    const authorization = localStorage.getItem('authorization');
    return !!authorization;
  }

  public logout(): void {
    localStorage.removeItem('authorization');
    this.router.navigateByUrl('login');
  }
}
