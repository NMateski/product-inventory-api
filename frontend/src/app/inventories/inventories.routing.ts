import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddInventoryComponent } from './add-inventory/add-inventory.component';
import { EditInventoryComponent } from './edit-inventory/edit-inventory.component';
import { InventoriesComponent } from './inventories.component';
import { InventoryListComponent } from './inventory-list/inventory-list.component';

const routes: Routes = [
  { path: '', component: InventoriesComponent, children: [
    { path: '', component: InventoryListComponent},
    { path: 'add', component: AddInventoryComponent},
    { path: ':inventoryId', children: [
      { path: '', component: EditInventoryComponent},
      { path: 'products', loadChildren: () => import('./products/products.module').then(m => m.ProductsModule)}
    ]},
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoriesRoutingModule { }
