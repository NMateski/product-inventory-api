import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inventories',
  template: '<app-authenticated-layout></app-authenticated-layout>'
})
export class InventoriesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
