import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Page } from 'src/app/shared';
import { ConfirmationPopupComponent } from 'src/app/shared/components';
import { Inventory, InventoryRequest } from '../inventories.domain';
import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.scss']
})
export class InventoryListComponent implements OnInit {

  public page: Page<Inventory> = new Page<Inventory>([], 0, 0);
  public bsModalRef: BsModalRef | undefined;

  constructor(private inventoryService: InventoryService, private router: Router, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.getPage(0, 10);
  }

  public onAdd(): void {
    this.router.navigateByUrl('inventories/add');
  }

  public onEdit(id: number): void {
    this.router.navigateByUrl(`inventories/${id}`);
  }

  public onDelete(id: number): void {
    this.bsModalRef = this.modalService.show(ConfirmationPopupComponent);
    this.bsModalRef.onHide.subscribe( _ => { 
        if (this.bsModalRef?.content.confirmed) {
          this.inventoryService.delete(id).subscribe({
            next: _ => this.getPage(0,10)
          });
        }
      }
    );
  }

  private getPage(page:number, size:number): void {
    this.inventoryService.getPageV2(page, size).subscribe(
      {next: response => this.page = response}
    )
  }

}
