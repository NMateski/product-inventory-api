export class Product {
    constructor(
        public id: number,
        public type: string,
        public price: number,
        public description: string,
        public inventory: string,
        public name: string
    ) { }
}

export class ProductRequest {
    constructor(
        public name: string,
        public type: string,
        public price: number,
        public description: string
    ) { }
}