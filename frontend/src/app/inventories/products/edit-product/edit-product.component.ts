import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../product.service';
import { ProductRequest } from '../products.domain';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  formGroup: FormGroup;
  private id: number | undefined;

  constructor(private router: Router, private productService: ProductService, private route: ActivatedRoute) {
    this.formGroup = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.maxLength(150)
      ]),
      type: new FormControl(null, [
        Validators.required,
        Validators.maxLength(50)
      ]),
      price: new FormControl(null, [
        Validators.required
      ]),
      description: new FormControl(null, [
        Validators.maxLength(1000)
      ])
    });
   }

  ngOnInit(): void {
    const rawId = this.route.snapshot.paramMap.get('id');
    if (rawId) {
      this.id = Number(rawId);
      this.productService.getById(this.id).subscribe({
        next: product => {
          this.formGroup.get('name')?.setValue(product.name);
          this.formGroup.get('type')?.setValue(product.type);
          this.formGroup.get('price')?.setValue(product.price);
          this.formGroup.get('description')?.setValue(product.description);
        }
      })
    }
  }

  public onSubmit(): void {
    if(this.formGroup.invalid) {
      console.log('Form is not valid!');
      console.log(this.formGroup.errors);
      return;
    }

    const name = this.formGroup.value.name;
    const type = this.formGroup.value.type;
    const price = this.formGroup.value.price;
    const description = this.formGroup.value.description;

    if (this.id) {
      this.productService.update(this.id, new ProductRequest(name, type, price, description)).subscribe({
        next: _ => this.router.navigate([`../`], {relativeTo: this.route})
      });
    }
  }

}
