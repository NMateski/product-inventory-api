import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Page } from 'src/app/shared';
import { ConfirmationPopupComponent} from 'src/app/shared/components';
import { ProductService } from '../product.service';
import { Product } from '../products.domain';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  public page: Page<Product> = new Page<Product>([], 0, 0);
  public bsModalRef: BsModalRef | undefined;
  public inventoryId: number | undefined;

  constructor(private productService: ProductService, private router: Router, private modalService: BsModalService,
     private route: ActivatedRoute) { }

  ngOnInit(): void {
    const inventoryId = this.route.snapshot.paramMap.get('inventoryId');
    if (inventoryId) {
      this.inventoryId = Number(inventoryId);
    }
    this.getPage(0,10);
  }

  public onAdd(): void {
    this.router.navigate(['./add'], { relativeTo: this.route });
    // this.router.navigateByUrl('inventories/');
  }

  public onEdit(id: number): void {
    this.router.navigateByUrl(`products/${id}`);
  }

  public onDelete(id: number): void {
    this.bsModalRef = this.modalService.show(ConfirmationPopupComponent);
    this.bsModalRef.onHide.subscribe( _ => { 
        if (this.bsModalRef?.content.confirmed) {
          this.productService.delete(id).subscribe({
            next: _ => this.getPage(0,10)
          });
        }
      }
    );
  }

  private getPage(page:number, size:number): void {
    if(!this.inventoryId) {
      console.log('Inventory ID is required!!!');
      return;
    }
    this.productService.getPage(this.inventoryId, page, size).subscribe(
      {next: response => this.page = response}
    )
  }

}
