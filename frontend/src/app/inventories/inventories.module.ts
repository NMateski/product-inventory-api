import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoriesComponent } from './inventories.component';
import { AddInventoryComponent } from './add-inventory/add-inventory.component';
import { EditInventoryComponent } from './edit-inventory/edit-inventory.component';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { InventoriesRoutingModule } from './inventories.routing';
import { SharedModule } from '../shared/shared.module';
import { InventoryService } from './inventory.service';


@NgModule({
  declarations: [
    InventoriesComponent,
    AddInventoryComponent,
    EditInventoryComponent,
    InventoryListComponent
  ],
  imports: [
    CommonModule,
    InventoriesRoutingModule,
    SharedModule
  ],
  providers: [
    InventoryService
  ]
})
export class InventoriesModule { }
