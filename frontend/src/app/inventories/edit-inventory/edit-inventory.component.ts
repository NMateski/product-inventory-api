import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryRequest } from '../inventories.domain';
import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-edit-inventory',
  templateUrl: './edit-inventory.component.html',
  styleUrls: ['./edit-inventory.component.scss']
})
export class EditInventoryComponent implements OnInit {

  formGroup: FormGroup;
  private id: number | undefined;

  constructor(private router: Router, private inventoryService: InventoryService, private route: ActivatedRoute) { 
    this.formGroup = new FormGroup({
      name: new FormControl(null, [
        Validators.maxLength(100)
      ]),
      location: new FormControl(null, [
        Validators.maxLength(50)
      ])
    });
  }

  ngOnInit(): void {
    const rawId = this.route.snapshot.paramMap.get('inventoryId');
    if (rawId) {
      this.id = Number(rawId);
      this.inventoryService.getById(this.id).subscribe({
        next: inventory => {
          this.formGroup.get('name')?.setValue(inventory.name);
          this.formGroup.get('location')?.setValue(inventory.location);
        }
      })
    }
  }

  public onSubmit(): void {
    if(this.formGroup.invalid) {
      console.log('Form is not valid!');
      console.log(this.formGroup.errors);
      return;
    }

    const name = this.formGroup.value.name;
    const location = this.formGroup.value.location;

    if (this.id) {
      this.inventoryService.update(this.id, new InventoryRequest(name, location)).subscribe({
        next: _ => this.router.navigateByUrl(`inventories`)
      });
    }

    // console.log(`Name: ${name}, Location: ${location}`);
  }

}
