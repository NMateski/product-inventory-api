CREATE TABLE public.groups
(
    id serial NOT NULL,
    group_name character varying(50) NOT NULL,
    created_at timestamp with time zone,
    CONSTRAINT groups_pkey PRIMARY KEY (id)
);

INSERT INTO groups VALUES (1, 'CLIENT_AND_ADMIN_GROUP', NULL);
INSERT INTO groups VALUES (2, 'CLIENT_ONLY_GROUP', NULL);

CREATE TABLE public.oauth_access_token
(
    token_id character varying(256),
    token bytea,
    authentication_id character varying(256) NOT NULL,
    user_name character varying(256),
    client_id character varying(256),
    authentication bytea,
    refresh_token character varying(256),
    CONSTRAINT oauth_access_token_pkey PRIMARY KEY (authentication_id)
);

CREATE TABLE public.oauth_client_details
(
    client_id character varying(256) NOT NULL,
    resource_ids character varying(256),
    client_secret character varying(256) NOT NULL,
    scope character varying(256),
    authorized_grant_types character varying(256),
    web_server_redirect_uri character varying(256),
    authorities character varying(256),
    access_token_validity integer,
    refresh_token_validity integer,
    additional_information character varying(4000),
    autoapprove character varying(256),
    CONSTRAINT oauth_client_details_pkey PRIMARY KEY (client_id)
);

INSERT INTO public.oauth_client_details VALUES ('public-android-client', NULL, '{bcrypt}$2a$10$3BBY.DDVmtcVsQqt5IEU8.FnRP9qYkKuaUjMHbcSYc.mWBHu9hj46', 'read,write', 'password,authorization_code,refresh_token', NULL, 'ROLE_CLIENT', 1800, 86400, NULL, NULL);
INSERT INTO public.oauth_client_details VALUES ('public-ios-client', NULL, '{bcrypt}$2a$10$3BBY.DDVmtcVsQqt5IEU8.FnRP9qYkKuaUjMHbcSYc.mWBHu9hj46', 'read,write', 'password,authorization_code,refresh_token', NULL, 'ROLE_CLIENT', 1800, 86400, NULL, NULL);
INSERT INTO public.oauth_client_details VALUES ('public-client', NULL, '{bcrypt}$2a$10$3BBY.DDVmtcVsQqt5IEU8.FnRP9qYkKuaUjMHbcSYc.mWBHu9hj46', 'read,write', 'password,authorization_code,refresh_token', NULL, 'ROLE_CLIENT', 1800, 86400, NULL, NULL);

CREATE TABLE public.oauth_client_token
(
    token_id character varying(256),
    token bytea,
    authentication_id character varying(256) NOT NULL,
    user_name character varying(256),
    client_id character varying(256),
    CONSTRAINT oauth_client_token_pkey PRIMARY KEY (authentication_id)
);

CREATE TABLE public.oauth_code
(
    code character varying(256),
    authentication bytea
);

CREATE TABLE public.oauth_refresh_token
(
    token_id character varying(256) NOT NULL,
    token bytea,
    authentication bytea,
    CONSTRAINT oauth_refresh_token_pkey PRIMARY KEY (token_id)
);

--Required: username, password, role, enabled, version
CREATE TABLE public.users
(
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    role character varying(255) NOT NULL,
    enabled boolean NOT NULL,
    created_at timestamp with time zone NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    date_of_birth timestamp with time zone,
    activation_key character varying(50),
    activation_key_created_at timestamp with time zone,
    reset_key character varying(50),
    reset_key_created_at timestamp with time zone,
    address character varying(255),
    city character varying(255),
    country character varying(255),
    version bigint,
    CONSTRAINT users_pkey PRIMARY KEY (username),
    CONSTRAINT users_email_key UNIQUE (email)
);

INSERT INTO public.users VALUES ('admin@test.com', '{bcrypt}$2a$10$FO52Hs5aeiK18Vj73tV6aucBi9gRu9tmE9XIGWxAKK0mSAz8VkYmS', 'admin@test.com', 'ADMIN', true, CURRENT_TIMESTAMP, 'ADMIN', 'ADMIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.users VALUES ('client@test.com', '{bcrypt}$2a$10$FO52Hs5aeiK18Vj73tV6aucBi9gRu9tmE9XIGWxAKK0mSAz8VkYmS', 'client@test.com', 'CLIENT', true, CURRENT_TIMESTAMP, 'CLIENT', 'CLIENT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

CREATE TABLE public.group_authorities
(
    group_id integer NOT NULL,
    authority character varying(50) NOT NULL,
    created_at timestamp with time zone,
    id serial NOT NULL,
    CONSTRAINT group_authorities_pkey PRIMARY KEY (id),
    CONSTRAINT fk_group_authorities_group FOREIGN KEY (group_id)
        REFERENCES public.groups (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE INDEX grp_auth_groups_idx
    ON public.group_authorities USING btree
    (group_id ASC NULLS LAST)
    TABLESPACE pg_default;

INSERT INTO group_authorities VALUES (1, 'AUTHORIZED_CLIENT', NULL, 1);
INSERT INTO group_authorities VALUES (1, 'AUTHORIZED_ADMIN', NULL, 2);
INSERT INTO group_authorities VALUES (2, 'AUTHORIZED_CLIENT', NULL, 3);

CREATE TABLE public.group_members
(
    id serial NOT NULL,
    username character varying(255) NOT NULL,
    group_id integer NOT NULL,
    created_at timestamp with time zone,
    CONSTRAINT group_members_pkey PRIMARY KEY (id),
    CONSTRAINT fk_group_members_group FOREIGN KEY (group_id)
        REFERENCES public.groups (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_group_members_users FOREIGN KEY (username)
        REFERENCES public.users (username) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE INDEX grp_mem_groups_idx
    ON public.group_members USING btree
    (group_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: grp_mem_users_idx

-- DROP INDEX public.grp_mem_users_idx;

CREATE INDEX grp_mem_users_idx
    ON public.group_members USING btree
    (username ASC NULLS LAST)
    TABLESPACE pg_default;

INSERT INTO group_members VALUES (1, 'admin@test.com', 1, NULL);
INSERT INTO group_members VALUES (2, 'client@test.com', 2, NULL);