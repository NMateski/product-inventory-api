package com.nikolamateski.productinventoryjpa.domain.inventory;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class InventorySearchRequest {

    public String name;
    public String location;
    public Pageable pageable;

    public InventorySearchRequest(String name, String location, Pageable pageable) {
        this.name = name;
        this.location = location;
        this.pageable = pageable;
    }

    public Specification<Inventory> generateSpecification() {
        Specification<Inventory> inventorySpecification = Specification.where(null);

//        List<Specification<Inventory>> specifications = new ArrayList<>();
        if (StringUtils.hasText(name)) {
            inventorySpecification = inventorySpecification.and(InventorySpecifications.byNameLiteralEquals(name));
        }

        if (StringUtils.hasText(location)) {
            inventorySpecification = inventorySpecification.and(InventorySpecifications.byLocationLiteralEquals(location));
        }

//        if (specifications.isEmpty()) {
//            return Optional.empty();
//        }

//        for (Specification<Inventory> specification : specifications) {
//            inventorySpecification = inventorySpecification.and(specification);
//        }

        return inventorySpecification;
    }
}
