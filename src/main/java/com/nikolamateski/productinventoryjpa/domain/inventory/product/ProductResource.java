package com.nikolamateski.productinventoryjpa.domain.inventory.product;


import com.nikolamateski.productinventoryjpa.utils.VoidDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/products")
public class ProductResource {

    private ProductService productService;
    public ProductResource(ProductService productService) {
        this.productService = productService;
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ProductDTO create(@RequestParam("inventoryId") final Integer inventoryId, @RequestBody final ProductCreateRequest request) {
        return productService.create(inventoryId,request);
    }
//
    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @PutMapping(path = "/{productId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ProductDTO update(@PathVariable("productId") final Integer productId,
                             @RequestBody final ProductRequest request) {
        return productService.update(productId, request);
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @DeleteMapping(path = "/{productId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable("productId") final Integer productId) {
        productService.delete(productId);
        return ResponseEntity.status(HttpStatus.OK).body(new VoidDTO(true));
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @GetMapping(path = "/{productId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ProductDTO getById(@PathVariable("productId") final Integer productId) {
        return productService.findById(productId);
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<ProductDTO> getPage(
            @RequestParam("inventoryId") final Integer inventoryId, @PageableDefault final Pageable pageable) {
        return productService.findPage(inventoryId, pageable);
    }
}
