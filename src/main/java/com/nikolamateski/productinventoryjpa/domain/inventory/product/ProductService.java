package com.nikolamateski.productinventoryjpa.domain.inventory.product;

import com.nikolamateski.productinventoryjpa.domain.inventory.InventoryRepository;
import com.nikolamateski.productinventoryjpa.exception.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ProductService {

    private InventoryRepository inventoryRepository;
    private ProductRepository productRepository;

    public ProductService(InventoryRepository inventoryRepository, ProductRepository productRepository) {
        this.inventoryRepository = inventoryRepository;
        this.productRepository = productRepository;
    }

    @Transactional
    public ProductDTO create(final Integer inventoryId, final ProductCreateRequest request) {
        var inventory = inventoryRepository.findById(inventoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Inventory with ID " + inventoryId + " not found!"));
        var product = new Product(request.type, request.price, request.description, request.name);
        product.inventory = inventory;
        return productRepository.save(product).toDTO();
    }

    @Transactional
    public ProductDTO update(final Integer productId, final ProductRequest request) {
        var product = productRepository.findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("Product with ID " + productId + " not found!"));
        product.type = request.type;
        product.price = request.price;
        product.description = request.description;
        product.name = request.name;
        return productRepository.save(product).toDTO();
    }

    @Transactional
    public void delete(final Integer productId) {
        var product = productRepository.findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("Product with ID " + productId + " not found!"));
        productRepository.delete(product);
    }

    @Transactional
    public ProductDTO findById(final Integer productId) {
        return productRepository.findById(productId)
                .map(Product::toDTO)
                .orElseThrow(() -> new ResourceNotFoundException("Inventory with " + productId + " does not exist!"));
    }

    @Transactional
    public Page<ProductDTO> findPage(final Integer inventoryId, final Pageable pageable) {
        return productRepository.findAllProducts(inventoryId, pageable)
                .map(Product::toDTO);
    }
}
