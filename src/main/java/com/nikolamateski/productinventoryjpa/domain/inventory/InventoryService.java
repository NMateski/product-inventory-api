package com.nikolamateski.productinventoryjpa.domain.inventory;

import com.nikolamateski.productinventoryjpa.domain.inventory.product.ProductDTO;
import com.nikolamateski.productinventoryjpa.exception.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class InventoryService {

    private InventoryRepository repository;

    public InventoryService(InventoryRepository repository) {
        this.repository = repository;
    }

    public InventoryDTO create(final InventoryRequest inventoryRequest) {
        var inventory = new Inventory(inventoryRequest.name, inventoryRequest.location);
        return repository.save(inventory).toDTO();
    }

    public List<InventoryDTO> findAll() {
        return repository.findAll().stream()
                .map(inventory -> inventory.toDTO())
                .collect(Collectors.toList());
    }

    public List<ProductDTO> findAllProductsByType(final String type, final Integer inventoryId) {
        return repository.findAllProductsByType(type, inventoryId).stream()
                .map(product -> product.toDTO())
                .collect(Collectors.toList());
    }

    public InventoryDTO findByNameNative(final String name) {
        var foundInventory = repository.findFirstByNameNative(name)
                .orElseThrow(() -> new RuntimeException("Inventory with name " + name + " is not found."));

        return new InventoryDTO(foundInventory.id, foundInventory.name, foundInventory.location);
    }

    public InventoryDTO update(final Integer inventoryId, final InventoryRequest request) {
        var inventory = repository.findById(inventoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Inventory with ID " + inventoryId + " not found!"));
        inventory.name = request.name;
        inventory.location = request.location;
        return repository.save(inventory).toDTO();
    }

    public void delete(final Integer inventoryId) {
        var inventory = repository.findById(inventoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Inventory with ID " + inventoryId + " not found!"));
        repository.delete(inventory);
//        return ResponseEntity.status(HttpStatus.OK).body("Inventory was successfully deleted!");
    }

    public Page<InventoryDTO> findPage(final InventorySearchRequest request) {

        return repository.findAll(request.generateSpecification(), request.pageable)
                .map(Inventory::toDTO);
    }

    public InventoryDTO findById(final Integer id) {
        return repository.findById(id)
                .map(Inventory::toDTO)
                .orElseThrow(() -> new ResourceNotFoundException("Inventory with " + id + " does not exist!"));
    }
}
