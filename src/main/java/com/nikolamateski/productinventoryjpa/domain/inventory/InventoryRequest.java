package com.nikolamateski.productinventoryjpa.domain.inventory;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class InventoryRequest {

    public String name;
    public String location;
}
