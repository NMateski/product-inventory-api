package com.nikolamateski.productinventoryjpa.domain.inventory;

import com.nikolamateski.productinventoryjpa.domain.inventory.product.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/inventories")
public class InventoryResource {

    private final InventoryService inventoryService;

    public InventoryResource(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public InventoryDTO create(@RequestBody final InventoryRequest inventoryRequest) {
        return inventoryService.create(inventoryRequest);
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<InventoryDTO> findPage(
            @RequestParam(value = "location", required = false) final String location,
            @RequestParam(value = "name", required = false) final String name, @PageableDefault Pageable pageable) {
        return inventoryService.findPage(new InventorySearchRequest(name, location, pageable));
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @GetMapping(path = "/{inventoryId}/find-products-by-type", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<ProductDTO> findAllProductsByType(@RequestParam("type") final String type,
                                                  @PathVariable("inventoryId") final Integer inventoryId) {
        return inventoryService.findAllProductsByType(type, inventoryId);
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @GetMapping(path = "/find-by-name/{name}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public InventoryDTO findByName(@PathVariable("name") final String name) {
        return inventoryService.findByNameNative(name);
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @PutMapping(path = "/{inventoryId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public InventoryDTO update(@PathVariable("inventoryId") final Integer inventoryId, @RequestBody final InventoryRequest request) {
        return inventoryService.update(inventoryId, request);
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @DeleteMapping(path = "/{inventoryId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable("inventoryId") final Integer inventoryId) {
        inventoryService.delete(inventoryId);
        return ResponseEntity.status(HttpStatus.OK).body("Inventory was successfully deleted!");
    }

//    @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @GetMapping(path = "/{inventoryId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public InventoryDTO getById(@PathVariable("inventoryId") final Integer inventoryId) {
        return inventoryService.findById(inventoryId);
    }
}