package com.nikolamateski.productinventoryjpa.domain.inventory.product;

import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
public class ProductRequest {

    public String type;
    public BigDecimal price;
    public String description;
    public String name;

    public ProductRequest(String type, BigDecimal price, String description, String name) {
        this.type = type;
        this.price = price;
        this.description = description;
        this.name = name;
    }
}
