package com.nikolamateski.productinventoryjpa.domain.inventory.product;

import com.nikolamateski.productinventoryjpa.domain.inventory.Inventory;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    public String type;
    public BigDecimal price;
    public String description;
    public String name;

    @ManyToOne
    @JoinColumn(name = "inventory_id")
    public Inventory inventory;

    public Product() {
    }

    public Product(String type, BigDecimal price, String description, String name) {
        this.type = type;
        this.price = price;
        this.description = description;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id.equals(product.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public ProductDTO toDTO() {
        return new ProductDTO(this.id, this.type, this.price, this.description, this.name);
    }
}
