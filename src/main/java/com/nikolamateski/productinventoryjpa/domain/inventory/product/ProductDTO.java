package com.nikolamateski.productinventoryjpa.domain.inventory.product;

import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@EqualsAndHashCode
public class ProductDTO {

    final public Integer id;
    final public String type;
    final public BigDecimal price;
    final public String description;
    final public String name;

    public ProductDTO(Integer id, String type, BigDecimal price, String description, String name) {
        this.id = id;
        this.type = type;
        this.price = price;
        this.description = description;
        this.name = name;
    }


}
