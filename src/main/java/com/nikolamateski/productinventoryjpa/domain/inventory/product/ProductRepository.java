package com.nikolamateski.productinventoryjpa.domain.inventory.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query("SELECT product FROM Product product INNER JOIN product.inventory inventory WHERE product.id = :productId AND inventory.id = :inventoryId")
    Optional<Product> findByIdAndInventoryId(Integer productId, Integer inventoryId);

    @Query("SELECT product FROM Product product INNER JOIN product.inventory inventory WHERE inventory.id = :inventoryId")
    Page<Product> findAllProducts(Integer inventoryId, Pageable pageable);

    public Page<Product> findByInventoryId(Integer inventoryId, Pageable pageable);
}
