package com.nikolamateski.productinventoryjpa.domain.inventory;

import com.nikolamateski.productinventoryjpa.domain.inventory.product.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface InventoryRepository extends JpaRepository<Inventory, Integer>, JpaSpecificationExecutor<Inventory> {

    @Query("SELECT product FROM Product product INNER JOIN product.inventory inventory WHERE product.type = :type AND inventory.id = :inventoryId")
    List<Product> findAllProductsByType(String type, Integer inventoryId);

    Optional<Inventory> findFirstByName(String name);

    @Query(nativeQuery = true, value = "SELECT * FROM inventory WHERE name = :name LIMIT 1")
    Optional<Inventory> findFirstByNameNative(String name);

    @Query(nativeQuery = true,
            value = "SELECT * FROM inventory " +
            " WHERE lower(name) = COALESCE(lower(:name), lower(name)) " +
            " and lower(location) = COALESCE(lower(:location), lower(location))",
            countQuery = "SELECT count(id) FROM inventory " +
            " WHERE lower(name) = COALESCE(lower(:name), lower(name)) " +
            " and lower(location) = COALESCE(lower(:location), lower(location))")
    Page<Inventory> findPage(final String location, final String name, final Pageable pageable);

}