package com.nikolamateski.productinventoryjpa.domain.inventory;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class InventoryDTO {

    final public Integer id;
    final public String name;
    final public String location;


    public InventoryDTO(Integer id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }
}
