package com.nikolamateski.productinventoryjpa.domain.systemuser.repository;

import com.nikolamateski.productinventoryjpa.domain.systemuser.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<Group, Integer> {

    Group findByGroupName(String groupName);


}
