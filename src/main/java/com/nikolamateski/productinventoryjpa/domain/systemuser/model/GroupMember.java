package com.nikolamateski.productinventoryjpa.domain.systemuser.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * The persistent class for the group_members database table.
 * 
 */

@Entity
@Table(name="group_members")
@NamedQuery(name="GroupMember.findAll", query="SELECT g FROM GroupMember g")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Access(AccessType.FIELD)
public class GroupMember implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Integer id;

	public String username;

	@Column(name="created_at")
	@CreationTimestamp
	public LocalDateTime createdAt;

	//bi-directional many-to-one association to Group
	@ToString.Exclude
	@ManyToOne(fetch = FetchType.LAZY)
	public Group group;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof GroupMember )) return false;
		return id != null && id.equals(((GroupMember) o).id);
	}
	@Override
	public int hashCode() {
		return 31;
	}

}