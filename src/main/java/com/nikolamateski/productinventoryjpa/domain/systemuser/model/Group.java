package com.nikolamateski.productinventoryjpa.domain.systemuser.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the groups database table.
 * 
 */

@Access(AccessType.FIELD)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="groups")
@NamedQuery(name="Group.findAll", query="SELECT g FROM Group g")
public class Group implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Integer id;

	@Column(name="group_name")
	public String groupName;

	@Column(name="created_at")
	@CreationTimestamp
	public LocalDateTime createdAt;

	//bi-directional many-to-one association to GroupAuthority
	@OneToMany(mappedBy="group", cascade={CascadeType.ALL})
	@ToString.Exclude
	public List<GroupAuthority> groupAuthorities = new ArrayList<>();

	//bi-directional many-to-one association to GroupMember
	@OneToMany(mappedBy="group", cascade={CascadeType.ALL})
	@ToString.Exclude
	public List<GroupMember> groupMembers = new ArrayList<>();

	@ManyToMany(mappedBy = "groups", fetch = FetchType.LAZY)
	@ToString.Exclude
	public Set<SystemUser> users = new HashSet<>();

	public GroupAuthority addGroupAuthority(GroupAuthority groupAuthority) {
		groupAuthorities.add(groupAuthority);
		groupAuthority.group = this;

		return groupAuthority;
	}

	public GroupAuthority removeGroupAuthority(GroupAuthority groupAuthority) {
		groupAuthorities.remove(groupAuthority);
		groupAuthority.group = null;

		return groupAuthority;
	}

	public GroupMember addGroupMember(GroupMember groupMember) {
		groupMembers.add(groupMember);
		groupMember.group = this;

		return groupMember;
	}

	public GroupMember removeGroupMember(GroupMember groupMember) {
		groupMembers.remove(groupMember);
		groupMember.group = null;

		return groupMember;
	}
}