package com.nikolamateski.productinventoryjpa.domain.systemuser.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;

public class AddClientRequest {

    @NotBlank(message = "First Name is required")
    public String firstName;

    @NotBlank(message = "Last Name is required")
    public String lastName;

    @NotBlank(message = "Username is required")
    public String userName;

    @NotBlank(message = "Email is required")
    @Email(message = "Email format is not valid")
    public String email;

    @NotNull(message = "Date of birth is required")
    public Instant dateOfBirth;

    @NotBlank(message = "Password is required")
    public String password;

    public String address;
    public String city;
    public String country;
}
