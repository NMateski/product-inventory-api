package com.nikolamateski.productinventoryjpa.domain.systemuser.repository;

import com.nikolamateski.productinventoryjpa.domain.systemuser.model.SystemUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SystemUserRepository extends JpaRepository<SystemUser, String> {

//    Optional<SystemUser> findByActivationKey(String activationKey);
//
//    Optional<SystemUser> findByResetKey(final String resetKey);

    Optional<SystemUser> findByEmail(final String email);

//    @Query("SELECT COUNT(player) FROM SystemUser player " +
//            " INNER JOIN Voucher voucher ON player = voucher.player " +
//            " WHERE player.username = :username ")
//    Integer playerHasRelations(String username);
//
//    @Query("SELECT new mk.cityclub.web.domain.systemuser.dto.UsersPerMonthContainer(" +
//                " COUNT(sysUser.username)," +
//                " '', " +
//                " MONTH(sysUser.createdAt) " +
//            " ) " +
//            " FROM SystemUser sysUser " +
//            " WHERE YEAR(sysUser.createdAt) = :year " +
//            " AND sysUser.registeredFrom = :platformType" +
//            " GROUP BY MONTH(sysUser.createdAt) ")
//    List<UsersPerMonthContainer> findUsersPerMonthForYear(int year, PlatformType platformType);
}
