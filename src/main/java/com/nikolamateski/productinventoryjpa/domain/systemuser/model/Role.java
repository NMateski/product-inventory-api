package com.nikolamateski.productinventoryjpa.domain.systemuser.model;

public enum Role {
	ADMIN,
	CLIENT
}