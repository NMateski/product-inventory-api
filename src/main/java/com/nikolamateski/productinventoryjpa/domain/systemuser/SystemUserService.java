package com.nikolamateski.productinventoryjpa.domain.systemuser;


import com.nikolamateski.productinventoryjpa.domain.systemuser.dto.AddClientRequest;
import com.nikolamateski.productinventoryjpa.domain.systemuser.dto.SystemUserDTO;
import com.nikolamateski.productinventoryjpa.domain.systemuser.model.Group;
import com.nikolamateski.productinventoryjpa.domain.systemuser.model.Role;
import com.nikolamateski.productinventoryjpa.domain.systemuser.model.SystemUser;
import com.nikolamateski.productinventoryjpa.domain.systemuser.repository.GroupRepository;
import com.nikolamateski.productinventoryjpa.domain.systemuser.repository.SystemUserRepository;
import com.nikolamateski.productinventoryjpa.exception.ResourceNotFoundException;
import com.nikolamateski.productinventoryjpa.exception.ResourceValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Validated
@RequiredArgsConstructor
@Slf4j
@Transactional
@Service
public class SystemUserService {

    static final int MAX_VALID_TIME_RESET_PASS = 24 * 60 * 60;
    static final String EMAIL_FROM = "no-reply@cityclub.mk";
    static final String EMAIL_MODEL = "user";

    private final PasswordEncoder passwordEncoder;
    private final SystemUserRepository systemUserRepository;
    private final GroupRepository groupRepository;

    public Page<SystemUserDTO> fetchPage(final Pageable pageable) {
        return systemUserRepository.findAll(pageable)
                .map(SystemUser::toDTO);
    }

    public SystemUserDTO fetchByUsername(final String username) {
        return systemUserRepository.findById(username)
                .map(SystemUser::toDTO)
                .orElseThrow(() -> new ResourceNotFoundException("User not found!!"));
    }
//
//
//
//    public UserView edit(final EditPlayerDetailsRequest request, final String username) {
//        final var city = findCityById(request.cityId);
//        final var systemUser = findUserById(username).update(request, city);
//        return systemUserRepository.save(systemUser).toView();
//    }

//

//
    public SystemUserDTO addClient(
            @Valid @NotNull(message = "{user.add-player-request-missing}")
            final AddClientRequest request) {
        log.debug("Add client with request [{}]", request);

        final var existingUserById = systemUserRepository.findById(request.userName);
        if (existingUserById.isPresent()) {
            throw new ResourceValidationException("User already registered!!");
        }

        final var existingUserByEmail = systemUserRepository.findByEmail(request.email);
        if (existingUserByEmail.isPresent()) {
            throw new ResourceValidationException("User already registered!!");
        }

        var user = new SystemUser();
        user.username = request.userName;
        user.email = request.email;
        user.password = passwordEncoder.encode(request.password);
        user.enabled = false;
        user.firstName = request.firstName;
        user.lastName = request.lastName;
        user.address = request.address;
        user.dateOfBirth = request.dateOfBirth;
        user.city = request.city;
        user.country = request.country;
        user.createdAt = Instant.now();
        user.role = Role.CLIENT;
        user.enabled = true;

        Group group = groupRepository.findByGroupName("CLIENT_ONLY_GROUP");
        user.addGroup(group);

        systemUserRepository.save(user);

        log.debug("Register player with request [{}] completed!", request);
        return user.toDTO();
    }
//
//    public void deletePlayer(final String username) {
//        final var systemUser = findUserById(username);
//        if (systemUserRepository.playerHasRelations(username) > 0) {
//            throw new ResourceValidationException(MessageCode.USER_HAS_VOUCHERS_CANT_DELETE, MessageCode.USER_HAS_VOUCHERS_CANT_DELETE.getCode());
//        }
//        systemUserRepository.delete(systemUser);
//    }
//
//    private EmailRequest generateUserActivatedEmailRequest(final SystemUser systemUser) {
//        return new EmailRequest(
//                EMAIL_FROM,
//                systemUser.email,
//                messageTranslator.getTextForKey(MessageCode.EMAIL_ACCOUNT_ACTIVATED_TITLE.getCode()),
//                null,
//                Map.of(EMAIL_MODEL, systemUser)
//        );
//    }
//
//    private EmailRequest generatePasswordResetEmailRequest(final SystemUser systemUser) {
//        return new EmailRequest(
//                EMAIL_FROM,
//                systemUser.email,
//                messageTranslator.getTextForKey(MessageCode.EMAIL_ACCOUNT_RESET_TITLE.getCode()),
//                null,
//                Map.of(EMAIL_MODEL, systemUser)
//        );
//    }
//
//    private EmailRequest generateActivationEmailRequest(final SystemUser systemUser) {
//        return new EmailRequest(
//                EMAIL_FROM,
//                systemUser.email,
//                messageTranslator.getTextForKey(MessageCode.EMAIL_ACCOUNT_ACTIVATION_TITLE.getCode()),
//                null,
//                Map.of(EMAIL_MODEL, systemUser)
//        );
//    }
//
//    private EmailRequest generateReferralEmailRequest(final SystemUser systemUser, final String referredToEmail) {
//        return new EmailRequest(
//                EMAIL_FROM,
//                referredToEmail,
//                messageTranslator.getTextForKey(MessageCode.EMAIL_REFERRAL_TITLE.getCode()),
//                null,
//                Map.of(EMAIL_MODEL, systemUser, "referralId", systemUser.username, "referredTo", referredToEmail)
//        );
//    }
//
//    private SystemUser findUserById(final String id) {
//        return systemUserRepository.findById(id)
//                .orElseThrow(() -> new ResourceNotFoundException(
//                        MessageCode.USER_NOT_FOUND, MessageCode.USER_NOT_FOUND.getCode()));
//    }
//
//    private SystemUser findUserByEmail(final String email) {
//        return systemUserRepository.findByEmail(email)
//                .orElseThrow(() -> new ResourceNotFoundException(
//                        MessageCode.USER_NOT_FOUND, MessageCode.USER_NOT_FOUND.getCode()));
//    }
//
//    private City findCityById(final long id) {
//        return cityRepository.findById(id)
//                .orElseThrow(() -> new ResourceValidationException(
//                        MessageCode.USER_REGISTER_CITY_REQUIRED, MessageCode.USER_REGISTER_CITY_REQUIRED.getCode()));
//    }
}
