package com.nikolamateski.productinventoryjpa.domain.systemuser.model;

import com.nikolamateski.productinventoryjpa.domain.systemuser.dto.SystemUserDTO;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Access(AccessType.FIELD)
public class SystemUser implements Serializable {

    @Id
    public String username;

    public String email;

    @ToString.Exclude
    public String password;

    @Enumerated(EnumType.STRING)
    public Role role;

    public boolean enabled;

    @Column(name = "created_at")
    public Instant createdAt;

    @Column(name = "activation_key")
    public String activationKey;

    @Column(name = "activation_key_created_at")
    public Instant activationKeyCreatedAt;

    @Column(name = "reset_key")
    public String resetKey;

    @Column(name = "reset_key_created_at")
    public Instant resetKeyCreatedAt;

    @Column(name = "first_name")
    public String firstName;

    @Column(name = "last_name")
    public String lastName;

    public String address;

    public String city;

    public String country;

    @Column(name = "date_of_birth")
    public Instant dateOfBirth;

    @Version
    public long version;

    @ToString.Exclude
    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    }, fetch = FetchType.EAGER)
    @JoinTable(name = "group_members",
            joinColumns = @JoinColumn(name = "username"),
            inverseJoinColumns = @JoinColumn(name = "group_id")
    )
    public Set<Group> groups = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SystemUser)) return false;
        return username != null && username.equals(((SystemUser) o).username);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    public SystemUserDTO toDTO() {
        return new SystemUserDTO(
                firstName,
                lastName,
                username,
                email,
                dateOfBirth,
                address,
                city,
                country
        );
    }

    public Group addGroup(final Group group) {
        groups.add(group);
        group.users.add(this);
        return group;
    }

    public Group removeGroup(final Group group) {
        groups.remove(group);
        group.users.remove(this);
        return group;
    }

//    public SystemUser update(final EditPlayerDetailsRequest request, final City city) {
//        address = request.address;
//        fullName = request.fullName;
//        phone = request.phone;
//        age = request.age;
//        isEmployed = request.isEmployed;
//        this.city = city;
//        return this;
//    }
}
