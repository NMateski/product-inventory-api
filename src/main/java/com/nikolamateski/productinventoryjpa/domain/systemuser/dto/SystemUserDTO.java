package com.nikolamateski.productinventoryjpa.domain.systemuser.dto;

import java.time.Instant;

public class SystemUserDTO {

    public String firstName;
    public String lastName;
    public String userName;
    public String email;
    public Instant dateOfBirth;
    public String address;
    public String city;
    public String country;

    public SystemUserDTO(String firstName, String lastName, String userName, String email, Instant dateOfBirth, String address, String city, String country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.city = city;
        this.country = country;
    }
}
