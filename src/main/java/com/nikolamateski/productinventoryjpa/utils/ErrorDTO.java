package com.nikolamateski.productinventoryjpa.utils;

public class ErrorDTO {

    public String message;

    public ErrorDTO(String message) {
        this.message = message;
    }
}
