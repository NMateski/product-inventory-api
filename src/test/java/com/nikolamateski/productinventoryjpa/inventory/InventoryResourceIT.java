package com.nikolamateski.productinventoryjpa.inventory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nikolamateski.productinventoryjpa.domain.inventory.InventoryDTO;
import com.nikolamateski.productinventoryjpa.domain.inventory.InventoryRequest;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class InventoryResourceIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private Flyway flyway;

    @BeforeEach
    private void beforeEach() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void given_valid_request__when_create__then_inventory_created() throws Exception {
        var request = new InventoryRequest("name", "location");

        var responseRaw = mockMvc.perform(post("/api/inventories")
                .content(mapper.writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON_VALUE)
//                .header("Authorization", "Bearer " + accessToken.getAccessToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        var result = mapper.readValue(responseRaw, InventoryDTO.class);
        var expectedResult = new InventoryDTO(result.id, request.name, request.location);

        assertNotNull(result);
        assertEquals(expectedResult, result);
    }
}
