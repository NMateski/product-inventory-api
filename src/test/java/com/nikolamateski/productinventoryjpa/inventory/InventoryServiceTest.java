package com.nikolamateski.productinventoryjpa.inventory;

import com.nikolamateski.productinventoryjpa.domain.inventory.*;
import com.nikolamateski.productinventoryjpa.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class InventoryServiceTest {

    @InjectMocks
    private InventoryService inventoryService;

    @Mock
    private InventoryRepository repository;

    @Test
    public void given_create_inventory_request__when_create_inventory__then_created_inventory() {
        when(repository.save(any(Inventory.class)))
                .thenAnswer(invocation -> invocation.getArgument(0, Inventory.class));

        var request = new InventoryRequest("name", "location");
        var expectedResult = new InventoryDTO(null, request.name, request.location);
        var result = inventoryService.create(request);

        assertNotNull(result);
        assertEquals(expectedResult, result);

    }

    @Test
    public void given_valid_inventory_id__when_update_inventory__then_update_inventory() {

        when(repository.findById(any()))
                .thenReturn(Optional.of(new Inventory()));
        when(repository.save(any(Inventory.class)))
                .thenAnswer(invocation -> invocation.getArgument(0, Inventory.class));
        var request = new InventoryRequest("name", "location");
        var expectedResult = new InventoryDTO(null, request.name, request.location);

        var result = inventoryService.update(expectedResult.id, request);

        assertNotNull(expectedResult);
        assertEquals(expectedResult, result);

    }

    @Test
    public void given_invalid_inventory_id__when_update_inventory__then_throw_exception() {
        when(repository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> inventoryService.update(0, new InventoryRequest()));
    }

    @Test
    public void given_valid_inventory_id__when_delete_inventory__then_delete_inventory() {
        var inventory = new Inventory();
        when(repository.findById(any()))
                .thenReturn(Optional.of(inventory));

        inventoryService.delete(0);

        verify(repository, times(1)).delete(inventory);

    }

    @Test
    public void given_invalid_inventory_id__when_delete_inventory__then_throw_exception() {
        when(repository.findById(any()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> inventoryService.delete(0));
    }

}
