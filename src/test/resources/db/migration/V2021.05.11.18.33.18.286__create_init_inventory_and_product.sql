INSERT INTO inventory(name, location) VALUES
('Name 1', 'Location 1'),
('Name 2', 'Location 2'),
('Name 3', 'Location 3');

INSERT INTO product(type, price, description, inventory_id, name) VALUES
('Type 1', 1.00, 'Description 1', (SELECT id FROM inventory WHERE name = 'Name 1'), 'Name 1'),
('Type 2', 2.00, 'Description 2', (SELECT id FROM inventory WHERE name = 'Name 1'), 'Name 2'),
('Type 3', 3.00, 'Description 3', (SELECT id FROM inventory WHERE name = 'Name 1'), 'Name 3'),

('Type 1', 4.00, 'Description 4', (SELECT id FROM inventory WHERE name = 'Name 2'), 'Name 4'),
('Type 2', 5.00, 'Description 5', (SELECT id FROM inventory WHERE name = 'Name 2'), 'Name 5'),
('Type 3', 6.00, 'Description 6', (SELECT id FROM inventory WHERE name = 'Name 2'), 'Name 6'),

('Type 1', 7.00, 'Description 7', (SELECT id FROM inventory WHERE name = 'Name 3'), 'Name 7'),
('Type 2', 8.00, 'Description 8', (SELECT id FROM inventory WHERE name = 'Name 3'), 'Name 8'),
('Type 3', 9.00, 'Description 9', (SELECT id FROM inventory WHERE name = 'Name 3'), 'Name 9');